package net.obs_cop.m.nakano.reports;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.query.JRCsvQueryExecuterFactory;

/**
 * 動作確認用.
 */
public class CsvApp {
  /**
   * 動作確認用.
   *
   * @throws JRException JasperReport exception.
   * @throws IOException JasperFillManager.fillReport
   */
  public static void main(String[] args) throws JRException, IOException {
    try (InputStream jasper = CsvApp.class.getResourceAsStream("/jasper/BlankCsv.jasper")) {
      HashMap<String, Object> parameters = new HashMap<String, Object>();
      parameters.put(JRCsvQueryExecuterFactory.CSV_SOURCE, args[0]);
      JasperPrint jasperPrint = JasperFillManager.fillReport(jasper, parameters);
      JasperExportManager.exportReportToPdfFile(jasperPrint, args[1]);
    }
  }
}
