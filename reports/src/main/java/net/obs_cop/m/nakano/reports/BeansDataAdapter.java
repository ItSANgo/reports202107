/**
 * see https://community.jaspersoft.com/wiki/how-create-and-use-jrdatasource-adapter
 */

package net.obs_cop.m.nakano.reports;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import org.apache.commons.beanutils.BeanMap;

/**
 * Adapter.
 */
public class BeansDataAdapter<BeanT extends Serializable> implements JRDataSource {

  /**
   * Beanのコンテナ.
   */
  private List<BeanT> beans = new ArrayList<BeanT>();

  /**
   * add a bean.
   */
  public void add(BeanT bean) {
    beans.add(bean);
  }

  /**
   * Variables to store the number of fields, and their names, in the report.
   */
  private HashMap<String, Integer> fieldsNumber = new HashMap<String, Integer>();

  /**
   * Method used to know if there are records to read.
   */
  private int lastFieldsAdded = 0;

  /**
   * Variable to store how much records were read.
   */
  private int counter = -1;

  @Override
  public boolean next() throws JRException {
    if (counter < beans.size() - 1) {
      ++counter;
      return true;
    }
    return false;
  }

  @Override
  public Object getFieldValue(JRField jrField) throws JRException {
    if (fieldsNumber.containsKey(jrField.getName()) == false) {
      fieldsNumber.put(jrField.getName(), lastFieldsAdded);
      ++lastFieldsAdded;
    }
    if (counter >= beans.size()) {
      return null;
    }
    BeanT bean = beans.get(counter);
    return new BeanMap(bean).get(jrField.getName());
  }
}
