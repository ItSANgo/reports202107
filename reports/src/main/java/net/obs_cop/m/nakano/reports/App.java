package net.obs_cop.m.nakano.reports;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

public class App 
{
    public static void main(String[] args) throws ClassNotFoundException,
    SQLException, JRException, IOException, IllegalAccessException,
    InvocationTargetException, NoSuchMethodException
    {
        // https://qiita.com/cherubim1111/items/1267731ef92f6779d160
        Class.forName("org.mariadb.jdbc.Driver");
        Connection conn = DriverManager.getConnection(
                "jdbc:mariadb://127.0.0.1/report_sample", "root", "");
        Statement stmt = conn.createStatement();
        String sql = "SELECT id, name FROM report_sample.users";
        ResultSet users = stmt.executeQuery(sql);
        UserDataAdapter src = new UserDataAdapter();
        while (users.next()) {
        	UserBean aUser = new UserBean();
        	aUser.setId(users.getInt(1));
        	aUser.setName(users.getString(2));
            src.add(aUser);
        }
        try (InputStream jasper = App.class.getResourceAsStream("/jasper/Blank_A4.jasper")) {
	        JasperPrint jasperPrint = JasperFillManager.fillReport(jasper, new HashMap<String, Object>(), src);
	        JasperExportManager.exportReportToPdfFile(jasperPrint, args[0]);
        }
    }
}
