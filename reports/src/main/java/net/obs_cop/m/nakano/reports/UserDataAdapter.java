package net.obs_cop.m.nakano.reports;

import net.sf.jasperreports.engine.JRDataSource;

/**
 * User Data Adapter.
 */
public class UserDataAdapter extends BeansDataAdapter<UserBean> {
  /**
   * Return an instance of the class that implements the custom data adapter.
   */
  public static JRDataSource getDataSource() {
    return new UserDataAdapter();
  }
}
