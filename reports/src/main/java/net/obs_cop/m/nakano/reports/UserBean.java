package net.obs_cop.m.nakano.reports;

import java.io.Serializable;

/**
 * User bean.
 *
 */
public class UserBean implements Serializable {
  private int id;
  private String name;

  /**
   * Getter.
   */
  public int getId() {
    return id;
  }

  /**
   * Setter.
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * Getter.
   */
  public String getName() {
    return name;
  }

  /**
   * Setter.
   */
  public void setName(String name) {
    this.name = name;
  }
}
